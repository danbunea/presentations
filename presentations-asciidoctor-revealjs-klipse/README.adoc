= asciidoctor-revealjs-klipse

Create slides from markup that contain interactive code snippets.

See the https://timothypratley.github.io/asciidoctor-revealjs-klipse/slides.html[live example].

Notice that you can click into the code blocks and change them, it re-evaluates on the fly.
This is handy for people who want to play around with your examples,
or if you just want nice syntax highlighting.

This is a template project that shows how you can produce interactive slides.
It combines:

1. https://asciidoctor.org/[Asciidoctor], a powerful toolchain for publishing
2. https://revealjs.com/[RevealJS], a slide deck generator
3. https://github.com/viebel/klipse[Klipse], an interactive client-side code evaluator.  


== Building

    ./bb.sh

Gets dependencies and builds

    ./watch.sh

Builds, watches for changes, rebuilds

