= asciidoctor-revealJS-klipse example Javascript: how is it run?
:copyright: Dan Bunea
:license: Eclipse Public License http://www.eclipse.org/legal/epl-v10.html
include::clj-10-min.attrs[]



== 1. Javascript: how is it run?

Now we will:
[%step]
* see a heap and a stack (runtime)
* question: is the runtime enough?
* learn about Web Api and Event Loop
* put runtime, web api, event loop and painting the dom all together

== 2. Heap/Stack

image::images/heap stack.png[Heap/Stack]

== 3. The stack (the code)

video::8aGhZQkoFbQ[youtube, start=331,end=360, options=autoplay]

== 4. Is the stack enough?


We also have async operations (in a single thread):
[%step]
* DOM events (onClick etc),
* setTimeout
* XmlHttpRequest

== 5. The full story is more complex

So the browser has the js runtime, web apis and the event loop

image::images/full image.png[Heap/Stack]

== 6. Demo




== 3. More examples Js/Promises

[source,javascript]
----
pi.startWith(model,"SAVE NEW CATALOG "+ template_cursor+" , "+no_of_pages)
        .then(render_status("loading"))
        .then(pi.change_multi({
            "storyboard.pages" : new_pages,
            "storyboard.order_by": window.parent.controller.model.searchcriterias.pages.order_by ? window.parent.controller.model.searchcriterias.pages.order_by : "",
            "storyboard.descending": window.parent.controller.model.searchcriterias.pages.descending ? window.parent.controller.model.searchcriterias.pages.descending : false
        }))
        .then(cc.change_storyboard_properties(name, page_size_cursor, is_first_different, is_landscape, is_odd_even, is_private, permissions))
        .then(REST.create_storyboard_catalog)
        .then(render)

----


== 15. Fin

Thank you

https://gitlab.com/danbunea

