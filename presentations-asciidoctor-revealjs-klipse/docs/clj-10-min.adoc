= asciidoctor-revealJS-klipse example clj in 10 minutes
:copyright: Dan Bunea
:license: Eclipse Public License http://www.eclipse.org/legal/epl-v10.html
include::clj-10-min.attrs[]



== 1. Clj/Cljs in 10 minutes experiment

Now we will:
[%step]
* learn data and functions
* know a few simple functions
* write some code
* use it on a server [backend] in Clojure
* use it on a web app [frontend] in ClojureScript

== 2. Data -JavaScript

[source,eval-javascript]
----
const dan = {
  name:"Dan",
  address:{
    city:"Barcelona"
  }
}

dan
----

== 3. Data - Clj/cljs

[source,eval-clojure]
----
(def dan {
  :name "Dan"
  :address {
    :city "Barcelona"
  }
})

dan
----

== 4. Functions - Js

Define and invoke:
[source,eval-javascript]
----

function add(a,b){
  return a+b;
}

add(3,4);
----


== 5. Functions - Clj/Cljs

Let's move some ( and get rid of some ;,:

[source,eval-clojure]
----

(defn add [a b]
  (+ a b))

(add 3 4)
----


== 6. Of course

It has:
[%step]
* Strings, ints, longs
* Lists, Arrays , Sets etc

* bla bla bla ... boring! :)


== 7. Let's add some properties

[source,eval-clojure]
(assoc {} :name "Dan")

or

[source,eval-clojure]

(assoc-in {} [:address :street] "Carrer Numancia")

== 8. Or update

[source,eval-clojure]
(assoc {:name "Dan"} :name "Dan Bunea")


== 9. Or get some values

[source,eval-clojure]
----
(def dan {:name "Dan"
:address {
:city "Timisoara, Romania"}})

(get dan :name)
----

or:

[source,eval-clojure]
(get-in dan [:address :city])

== 10. But there are better ways ->

[source,eval-clojure]

(:name dan)



[source,eval-clojure]
(-> dan
:name )



[source,eval-clojure]
(-> dan
:address
:city)


== 11. Backend

If we wanted to change the city in a function
[source,eval-clojure]
----
(defn move [city street no]
  (-> dan
    (assoc-in [:address :city] city)
    (assoc-in [:address :street] street)
    (assoc-in [:address :no] no)))

(move "Barceloooona!" "Numancia" 4)
----

== 12. A server

[source]
-----
(GET "/move" []
     :query-params [city :- s/Str
                    street :- s/Str
                    no :- s/Num]
    (-> dan
      (assoc-in [:address :city] city)
      (assoc-in [:address :street] street)
      (assoc-in [:address :no] no)
      (ok )))
-----

Heroku: https://floating-tundra-40445.herokuapp.com
Gitlab: https://gitlab.com/danbunea/clj-10-minutes/blob/master/src/clj_10_min_server/handler.clj


== 11. Atoms - like variables but better

Define:

[source,eval-clojure]
----
(def model (atom {:name "Dan"}))
model
----

Value:
[source,eval-clojure]
@model

Change:
[source,eval-clojure]
----
(swap! model assoc :name "Dan Bunea")
@model
----

== 12. Web applications - Reagent/React

[source,eval-reagent]
----
(require '[reagent.core :as r])


(defn name-component [name]
  [:h2
    {:style {:border "1px solid red"
    :margin 3}}
    name
    ])


[name-component "I am a component"]

----

== 13. Proper web application

[source,eval-reagent]
----
(def model (r/atom {
                    :name "Dan"
                    :address {
                              :city "Timisoara, Romania"
                              }}))
(defn commit [new-value]
  (reset! model new-value))

;;CONTROLLER
(defn move [city street no]
  (-> @model
    (assoc-in [:address :city] city)
    (assoc-in [:address :street] street)
    (assoc-in [:address :no] no)
    commit
    ))

;;VIEW
(defn participant-component []
  [:div
   [:p "Name is: " (:name @model)]
   [:p "Living in: " (-> @model
                       :address
                       :city)]
   [:input
    {
     :type "button"
     :value "Move"
     :on-click #(move "Barcelona" "Numancia" 1)
     }]
   ]
  )

[participant-component ]


----

Y ya esta! :)
How would we add the age and later change it?

== 13. Summary

We learned
[%step]
* def/defn
* assoc/assoc-in
* get, get-in ...
* -> (pipe)
* atom, swap! and reset!

== 14. Summary

We used them:
[%step]
* on a server [backend] in Clojure
* on a web app [frontend] in ClojureScript


== 15. Fin

Now imagine WHAT all your best practices, combined with a fantastically productive programming language could do :)
Thank you

https://gitlab.com/danbunea

